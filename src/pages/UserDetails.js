import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import {Button} from 'react-bootstrap'
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Swal from 'sweetalert2'


export default function UserDetails() {

	const [show, setShow] = useState(false);
  	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);
  	const [newPassword, setNewPassword] = useState("")
  	const [isActive, setIsActive] = useState(false)

	const {user, setUser} = useContext(UserContext)

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [mobileNumber, setMobileNumber] = useState("")

	const changePassword = (event) => {
		event.preventDefault()
		console.log(newPassword)
		console.log(user)
		console.log(localStorage)

		fetch(`${process.env.REACT_APP_API_URL}/users/password-reset`, {
    	method: 'PATCH',
      	headers: {
      		//authorization causes the error. If I remove authorization, it returns "no token detected" and the password change does not take effect
      	Authorization : `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        newPassword: newPassword
      })
    }).then(response => {
			console.log(response)
			return response.json()
		}).then(result => {
			console.log(result)

			if (result !== false) {

        Swal.fire({
              title: 'Success!',
              icon: 'success',
              text: 'You have successfully changed your password'
            })
      } else {
          Swal.fire({
              title: 'Oops!',
              icon: 'error',
              text: "Something went wrong. Please try again."
            })
      }

		})

	}
	
	useEffect(() => {
		if (newPassword !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [newPassword])


	if (user.id !== null) {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(response => response.json())
		.then(result => { 
			setFirstName(result.firstName)
			setLastName(result.lastName)
			setEmail(result.email)
			setMobileNumber(result.mobileNo)
		})
	} else {
		return(
				<h1>Please log in first.</h1>
			)
	}

	return(
		<>
			<h1>Your details</h1>
			<h3>First name: {firstName}</h3>
			<h3>Last name: {lastName}</h3>
			<h3>Email: {email}</h3>
			<h3>Mobile number: {mobileNumber}</h3>

			{/*we will change password here*/}
			<Button variant="primary" onClick={handleShow}>
			        Change Password
			</Button>

			<Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Change Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="newPassword">
              <Form.Label>New Password</Form.Label>
              <Form.Control
                type="text"
                placeholder="New password"
                value={newPassword}
                onChange={event => setNewPassword(event.target.value)}
                autoFocus
              />
            </Form.Group>
          </Form>

          {/*<Form>
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="text"
                placeholder="Email"
                value={email}
                onChange={event => setEmail(event.target.value)}
                autoFocus
              />
            </Form.Group>
          </Form>*/}

        </Modal.Body>

        { isActive ?
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Cancel
            </Button>
            <Button variant="primary" onClick={(event) => {changePassword(event)}}>
              Submit
            </Button>
          </Modal.Footer>
            :
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose} disabled>
              Cancel
            </Button>
            <Button variant="primary" type="submit" id="submitBtn" disabled>
              Submit
            </Button>
          </Modal.Footer>
          }

      </Modal>
		</>
		)
}