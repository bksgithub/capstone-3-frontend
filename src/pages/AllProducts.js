import ProductCard from '../components/ProductCard'
import Loading from '../components/Loading'
import {useEffect, useState} from 'react'


export default function AllProducts(){
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	useEffect(() => {
		// Sets the loading state to true
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/products/`, {
			headers: {
				method: "GET",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(response => response.json())
		.then(result => { 
			console.log(result)
			setProducts(
				result.map(product => {
					return (
						<ProductCard key={product._id} product={product}/>
					)
				})
			)

			// Sets the loading state to false
			setIsLoading(false)
		})
	}, [])

	return(
		(isLoading) ?
			<Loading/>
		:
			<>
				{products}
			</>
	)
}