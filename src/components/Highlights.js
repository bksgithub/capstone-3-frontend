import { Row, Col, Card } from 'react-bootstrap'
import Image from 'react-bootstrap/Image'

export default function Highlights(){
	return(
		<Row>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Don't start from scratch</h2>
		                </Card.Title>
		                <Card.Text>
		                    We all know how hard it is to start from scratch. Our goal is to solve that problem so that players can enjoy the game more. It saves you time, energy and resources by not playing from the beginning. 
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>What you see is what you get</h2>
		                </Card.Title>
		                <Card.Text>
		                    The accounts on sale are updated real time. Our system allows vendors to update their account information right after they did some changes to it. For example, changes in level, changes in zeny, obtained new cards, items, resources, etc.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Our community is the best</h2>
		                </Card.Title>
		                <Card.Text>
		                    We have a community to help you continue the progress of the account that you bought. The community gives tips and tricks on how to progress on your new accounts and how to avoid getting stuck. They also give tips on how to get the most out of your budget so you can get the best account for you.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>
	)
}