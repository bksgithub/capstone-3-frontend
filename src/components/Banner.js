import {Button, Row, Col} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'

export default function Banner(){

	const navigate = useNavigate()

	return(
		<Row>
			<Col className="p-5">
				<h1>Ragnarok Account Buy and Sell</h1>
				<p>Happy gaming!</p>
				<Button className = "me-3" variant="primary" onClick={() => navigate('/products/active')}>Explore</Button>
				<Button className = "me-auto" variant="primary" onClick={() => navigate('/products/6354152a6e4042cd91cee316')}>Featured Account</Button>
				<Button className = "view-profile" variant="primary" onClick={() => navigate('/users/details')}>View Profile</Button>
			</Col>
			
		</Row>
	)
}