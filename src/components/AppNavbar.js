import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink } from 'react-router-dom'
import {useEffect, useContext} from 'react'
import UserContext from '../UserContext'

export default function AppNavbar(){

  //this is my global state
  const {user, setUser} = useContext(UserContext)

  //this is to check the output of user
  console.log(user)

  if (user.id && user.isAdmin === false) {
  return (
      <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/">Ragnarok Account Buy and Sell</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link as={NavLink} to="/products/active">Accounts on Sale</Nav.Link>
        </Nav>
      <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/checkout">Checkout</Nav.Link>
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
            </Nav>
      </Navbar.Collapse>
    </Navbar>
    )
} else if (user.id && user.isAdmin === true) {
  return (
  <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/admin">Welcome Admin</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link as={NavLink} to="/products/">Get all products</Nav.Link>
        </Nav>
      <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
            </Nav>
      </Navbar.Collapse>
    </Navbar>
    )
} else {
  return(
      <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/">Ragnarok Account Buy and Sell</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link as={NavLink} to="/products/active">Accounts on Sale</Nav.Link>
        </Nav>
      <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
      </Nav>
      </Navbar.Collapse>
    </Navbar>
    )
}
}

