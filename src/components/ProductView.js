import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';


export default function ProductView() {

	const handleClose = () => setShow(false);
 	const handleShow = () => setShow(true);
 	const [show, setShow] = useState(false);
 	const [isActive, setIsActive] = useState(false)

	// Gets the courseId from the URL of the route that this component is connected to. '/courses/:courseId'
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);  

	const [productName, setProductName] = useState("")
  const [productDescription, setProductDescription] = useState("")
 	const [productPrice, setProductPrice] = useState(0)
 	const [quantity, setQuantity] = useState(1)


 	const activateProduct = (productId) => {

 		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
 			method: 'PATCH',
	      	headers: {
	        Authorization: `Bearer ${localStorage.getItem('token')}`,
	        "Content-Type": "application/json"
		    },
		    body: JSON.stringify({
		    	isActive: true
		    })
  }).then(response => response.json()).then((result) => {
  	console.log(result)
  	Swal.fire({
              title: 'Success!',
              icon: 'success',
              text: 'You have successfully activated a product!'
            })

        navigate('/admin')
  })
 	}

 	const deactivateProduct = (productId) => {

 		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
 			method: 'PATCH',
	      	headers: {
	        Authorization: `Bearer ${localStorage.getItem('token')}`,
	        "Content-Type": "application/json"
		    }, 
		    body: JSON.stringify({
		    	isActive: false
		    })
  }).then(response => response.json()).then((result) => {
  	Swal.fire({
              title: 'Success!',
              icon: 'success',
              text: 'You have successfully deactivated a product!'
            })

        navigate('/admin')
  })
 	}

 	// useEffect(() => {

 	// }, [activate, deactivate])

	const updateProduct = (productId) => {

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
    	method: 'PATCH',
      	headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        "Content-Type": "application/json"
      }, 
      body: JSON.stringify({
        name: productName,
        description: productDescription,
        price: productPrice
      })
    }).then(response => {
    	return response.json()
    }).then((result) => {

      if (result !== false) {
        setProductName("")
        setDescription("")
        setPrice(0)

        Swal.fire({
              title: 'Success!',
              icon: 'success',
              text: 'You have successfully updated a product!'
            })

        navigate('/admin')
      } else {
          Swal.fire({
              title: 'Oops!',
              icon: 'error',
              text: "Something went wrong. Please try again."
            })
      }
    })
    // We may put the error alert in a catch block
  }

  useEffect(() => {
    if(productName !== "" && productDescription !== "" && productPrice !== null){
      // Enables the submit button if the form data has been verified
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [productName, productDescription, productPrice])

	const buy = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/create-order`, {
          method: 'POST',
          headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          userId: user.id,
          productId: productId,
          quantity: quantity
        })
  }).then(response => response.json()).then(result => {
    console.log(result)
  })


		fetch(`${process.env.REACT_APP_API_URL}/users/buy`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				userId: user.id
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "You bought the account successfully!"
				})

				navigate('/products/active')
			} else {
				console.log(result)

				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
		// We may put the error alert in a catch block
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [productId])

	if (user.id !== null && user.admin === false) {
			return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
					{/*<Card.Img variant="top" src="../productImg/nealDR.PNG" alt="nealDR image"/>*/}
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							
							{	user.id === null  ? 
								<Link className="btn btn-danger btn-block" to="/login">Log in to buy account</Link>
								:
									<Button variant="primary" onClick={() => buy(productId)}>Buy</Button>
									
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
		
	} else if (user.id !== null && user.isAdmin === true) {
		return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Img variant="top" src="../productImg/nealDR.PNG" alt="nealDR image"/>
							
							{	user.id !== null ? 
									
    <>
      <Button variant="primary" onClick={handleShow}>
        Update Product
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="productName">
              <Form.Label>IGN</Form.Label>
              <Form.Control
                type="text"
                placeholder="Name"
                value={productName}
                onChange={event => setProductName(event.target.value)}
                autoFocus
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="productDescription"
            >
              <Form.Label>Description</Form.Label>
              <Form.Control as="textarea" rows={3} placeholder="Say something about your character." type="text" value={productDescription}
                onChange={event => setProductDescription(event.target.value)}/>
            
            </Form.Group>
            <Form.Group className="mb-3" controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Price"
                value={productPrice}
                onChange={event => setProductPrice(event.target.value)}
                autoFocus
              />
            </Form.Group>
          </Form>
        </Modal.Body>
          { isActive ?
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Cancel
              </Button>
              <Button variant="primary" type="submit" id="submitBtn" onClick={() =>updateProduct(productId)
              }>
                Submit
              </Button>
              <Button variant="secondary" onClick={() => deactivateProduct(productId)}>
                Deactivate
              </Button>
              <Button variant="primary" onClick={() => activateProduct(productId)}>
                Activate
              </Button>
            </Modal.Footer>
              :
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose} disabled>
                Cancel
              </Button>
              <Button variant="primary" type="submit" id="submitBtn" disabled>
                Submit
              </Button>
            </Modal.Footer>
            }
      </Modal>
    </>
  
								:
									<Link className="btn btn-danger btn-block" to="/login">Log in to buy account</Link>
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
	} else {
		return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
					<Card.Img variant="top" src="../productImg/nealDR.PNG" alt="nealDR image"/>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							
							{	user.id === null  ? 
								<Link className="btn btn-danger btn-block" to="/login">Log in to buy account</Link>
								:
									<Button variant="primary" onClick={() => buy(productId)}>Buy</Button>
									
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
	}

	
}




