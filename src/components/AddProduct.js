import {Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import {useContext, useState, useEffect} from 'react'
import Swal from 'sweetalert2'
import {useNavigate} from 'react-router-dom'
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

// export default function UpdateProduct() {

// }


export default function AddProduct() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const navigate = useNavigate()

  const [productName, setProductName] = useState("")
  const [description, setDescription] = useState("")
  const [price, setPrice] = useState(0)
  const [isActive, setIsActive] = useState(false)


  const createProduct = (event) => {
    event.preventDefault()
    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        "Content-Type": "application/json"
      }, 
      body: JSON.stringify({
        name: productName,
        description: description,
        price: price
      })
    }).then(response => response.json()).then((result) => {

      if (result !== false) {
        setProductName("")
        setDescription("")
        setPrice(0)

        Swal.fire({
              title: 'Success!',
              icon: 'success',
              text: 'You have successfully created a product!'
            })

        navigate('/admin')
      } else {
          Swal.fire({
              title: 'Oops!',
              icon: 'error',
              text: "Something went wrong. Please try again."
            })
      }
    })
    // We may put the error alert in a catch block
  }

  useEffect(() => {
    if(productName !== "" && description !== "" && price !== null){
      // Enables the submit button if the form data has been verified
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [productName, description, price])

  // console.log(productName, description, price)


  // useEffect(() => {
  //  setProductName(productName)
  //  setDescription(description)
  //  setPrice(price)
  // }, [productName, description, price])

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Add Product
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit = {event => createProduct(event)}>
            <Form.Group className="mb-3" controlId="productName">
              <Form.Label>IGN</Form.Label>
              <Form.Control
                type="text"
                placeholder="Name"
                value={productName}
                onChange={event => setProductName(event.target.value)}
                autoFocus
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="description"
            >
              <Form.Label>Description</Form.Label>
              <Form.Control as="textarea" rows={3} placeholder="Say something about your character." type="text" value={description}
                onChange={event => setDescription(event.target.value)}/>
            
            </Form.Group>
            <Form.Group className="mb-3" controlId="price">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Price"
                value={price}
                onChange={event => setPrice(event.target.value)}
                autoFocus
              />
            </Form.Group>
          </Form>
        </Modal.Body>
          { isActive ?
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Cancel
              </Button>
              <Button variant="primary" type="submit" id="submitBtn" onClick={event => createProduct(event)}>
                Submit
              </Button>
            </Modal.Footer>
              :
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose} disabled>
                Cancel
              </Button>
              <Button variant="primary" type="submit" id="submitBtn" disabled>
                Submit
              </Button>
            </Modal.Footer>
            }
      </Modal>
    </>
  );
}


// {
//      method: 'POST',
//      headers: {
//        "Content-Type": "application/json",
//        Authorization: `Bearer ${localStorage.getItem('token')}`
//      },
//      body: JSON.stringify({
//        name: productName,
//        description: description,
//        price: price
//      })
//    }