import React from "react"

//initializes a react context
const UserContext = React.createContext()
//gives us ability to provide specific context through a component

export const UserProvider = UserContext.Provider 
export default UserContext