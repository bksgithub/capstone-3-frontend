// import logo from './logo.svg';
import './App.css';
import './index.css'
import {BrowserRouter as Router, Route, Routes} from "react-router-dom"
import AppNavbar from './components/AppNavbar'
import Register from './pages/Register'
import {Container} from "react-bootstrap"
import {UserProvider} from "./UserContext"
import {useState} from "react"
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products'
import ProductView from './components/ProductView'
import Error from './pages/Error'
import AdminDashboard from './pages/AdminDashboard'
import Checkout from './pages/Checkout'
import AllProducts from './pages/AllProducts'
import UserDetails from './pages/UserDetails'


function App() {

  const [user, setUser] = useState({
  id: null,
  isAdmin: null
})

const unsetUser = () => {
  localStorage.clear()
}

  return (
      <>
      <UserProvider value = {{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path = "/register" element = {<Register/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path = "/" element = {<Home/>}/>
              <Route path="/logout" element={<Logout/>} />
              <Route path="/products/active" element={<Products/>}/>
              <Route path="/products/:productId" element={<ProductView/>}
              />
              <Route path="*" element={<Error/>}/>
              <Route path="/admin" element={<AdminDashboard/>}/>
              <Route path="/checkout" element={<Checkout/>}/>
              <Route path="/products/" element={<AllProducts/>}/>
              <Route path="/orders/create-order" element={<Checkout/>}/>
              <Route path="/users/details" element={<UserDetails/>}/>
            </Routes>
          </Container>
        </Router>
      </UserProvider>
      </>
    )
}

export default App;
